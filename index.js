const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const cors = require('cors');

const uuidv4 = require('uuid/v4');

require('dotenv').config();
const slackToken = process.env.SLACK_BOT_TOKEN;
const slack = require('slack');

const port = 3001;

const fs = require('fs');
const dataDir = './data';
const scheduleFile = `${dataDir}/jobs.json`;
var scheduled = {};
var timeouts = {};

var channelList = {};
slack.conversations.list({token: slackToken})
.then( res => {
	res.channels.forEach(it => {
		channelList[it.id] = it.name;
	});
})
.catch( e => {
	console.log(e);
});

// Load existing jobs
fs.readFile(scheduleFile, 'utf8', (err, data) => {
	if (!err) {
		scheduled = JSON.parse(data);
		reschedule();
	} else {
		fs.readFile(`${scheduleFile}.bak`, 'utf8', (err, data) => {
			if (!err) {
				scheduled = JSON.parse(data);
				reschedule();
			}
		});
	}
});

const domainWhitelist = ['http://localhost:3000', 'https://task-manager.localtunnel.me', 'https://task-manager-api.localtunnel.me', 'https://hooks.slack.com'];
const corsSettings = {
	origin: (origin, callback) => {
		if (domainWhitelist.indexOf(origin) !== -1) { //TODO: Use regex (https?, /.*)
			callback(null, true)
		} else {
			console.log(origin)
			callback(new Error('Not on CORS whitelist'))
		}
	}
};
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Slack commands
app.post('/slack/tasks', (req, res) => {
	/*
	req.body.channel_name
	req.body.user_name
	req.body.command
	req.body.text
	*/

	var queued = 'Following tasks are queued:\n';

	Object.keys(scheduled).forEach((key) => {
		queued += `[${channelList[scheduled[key].channel]}] ${scheduled[key].date} ${scheduled[key].time} - ${scheduled[key].msg}\n`
	});

	res.send({
		"text": queued
	});
});


/* TODO add other commands
app.post('/slack/job', (req, res) => {
	req.body.channel_name
	user_name
	command
	text
	var time = 0


	res.send({
		"text": 'Message',
	    "attachments": [
	        {
	            "text": `${message}`
	        }
	    ]
	})
});
*/

// Internal commands
app.get('/api/jobList', (req, res) => {
	res.send(scheduled);
});

app.get('/api/channelList', (req, res) => {
	slack.conversations.list({token: slackToken})
	.then( r => {
		res.send(r)
	})
	.catch( e => {
		console.log(e);
		res.send({
			"result": "error",
			"description": e
		})
	});
});

app.post('/api/job', (req, res) => {
	createJob(req.body.date, req.body.time, req.body.channel, req.body.msg);
	res.send(scheduled);
});

app.post('/api/removeJob', (req, res) => {
	clearTimeout(timeouts[req.body.id]);
	delete timeouts[req.body.id];
	delete scheduled[req.body.id];
	saveJobs();
	res.send(scheduled);
});

reschedule = () => {
	Object.keys(timeouts).forEach((key) => {
		clearTimeout(timeouts[key]);
	});
	delete timeouts;

	var old = [];
	Object.keys(scheduled).forEach((key) => {
		let it = scheduled[key];

		let when = Date.parse(`${it.date}T${it.time}`);
		let fromNow = when - Date.now();

		if (fromNow > 0) {
			setTimeout(notify, fromNow, key);
		}
	});

	old.forEach((key) => {
		notify(key);
	});
}

saveJobs = async () => {
	if (!fs.existsSync(dataDir)) {
	  fs.mkdirSync(dataDir);
	}

	try {
		fs.renameSync(scheduleFile, `${scheduleFile}.bak`);
	} catch (e) {}
	fs.writeFile(scheduleFile, JSON.stringify(scheduled), 'utf8', (err) => {
		if (err) console.log(err);
	});
}

notify = (key) => {
	if (key in scheduled) {
		slack.chat.postMessage({
			token: slackToken,
			channel: scheduled[key].channel,
			text: scheduled[key].msg
		});
		delete timeouts[key];
		delete scheduled[key];
		saveJobs();
	}
}

createJob = async (aDate, aTime, aChannel, aMsg) => {
	const key = uuidv4();

	let when = Date.parse(`${aDate}T${aTime}`);
	let fromNow = when - Date.now();

	timeouts[key] = setTimeout(notify, fromNow, key);

	scheduled[key] = {
		date: aDate,
		time: aTime,
		channel: aChannel,
		msg: aMsg
	};
	saveJobs();
}

app.listen(port, () => console.log(`Example app listening on port ${port}`));
