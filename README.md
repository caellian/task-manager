# task-manager v0.1.0
Simple task management app for Slack.

## Requirements:
- node

Dev env:

- localtunnel

npm install -g localtunnel

## Known flaws:
- All scheduled messages clear when the server stops.
Can be fixed by storing jobs in a DB or using a working library.

## Running

Install dependencies:
yarn install && cd client && yarn install && cd ..

Run in development environment:
yarn devlt
