import axios from 'axios';

const Instance = axios.create({
	baseURL: 'https://task-manager-api.localtunnel.me',
	//timeout: 1000, // Slack can be a tiny bit slow sometimes.
	headers: {
		'Access-Control-Allow-Origin': 'https://task-manager.localtunnel.me'
	}
});

export default Instance;
