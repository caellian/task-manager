import React, { Component } from 'react';

import Axios from './AxiosInstance';

import './App.css';

class App extends Component {
  state = {
    jobList: [],
    channelList: {},

    jobDate: '2019-01-01',
    jobTime: '12:00',
    jobChannel: null,
    jobMessage: ''
  };

  componentWillMount() {
    Axios.get('/api/jobList')
      .then(res => this.setState({ jobList: res.data }))
      .catch(err => console.log(err));

    Axios.get('/api/channelList')
      .then(res => {
        var channels = {};
        res.data.channels.forEach(it => {
          channels[it.id] = it.name;
        });
        this.setState({ channelList: channels, jobChannel: Object.keys(channels)[0] });
      })
      .catch(err => console.log(err));
  }

  channelSelected = (event) => {
    this.setState({ jobChannel: event.target.value })
  }

  createJob = (event) => {
    event.preventDefault();
    if (this.state.jobChannel != null) {
      Axios.post('/api/job', {
        date: this.state.jobDate,
  			time: this.state.jobTime,
  			channel: this.state.jobChannel,
  			msg: this.state.jobMessage
  		})
      .then(res => {
        this.setState({ jobList: res.data })
      })
  		.catch(err => console.log(err));
    }
  }

  render() {
    var table = [];

		var rows = [];
		var counter = 0;

    Object.keys(this.state.jobList).forEach( key => {
      let job = this.state.jobList[key];
      rows.push(
        <tr key={counter++}>
          <td>{job.date} {job.time}</td>
          <td>{this.state.channelList[job.channel]}</td>
          <td>{job.msg}</td>
          <td><button onClick={() => {
            Axios.post('/api/removeJob', {
              id: key
            })
            .then(res => {
              this.setState({ jobList: res.data })
            })
            .catch(err => console.log(err));
          }}>Cancel</button></td>
        </tr>
        )
    });

		if (rows.length > 0) {
			table.push(<h1 key='jobs-label'>Jobs:</h1>);
			table.push(
				<table key='jobs-table'>
					<tbody>
						<tr>
							<th>Time</th>
							<th widht='10%'>Channel</th>
							<th width='70%'>Message</th>
							<th/>
						</tr>
						{rows}
					</tbody>
				</table>
			);
		} else {
			table.push(<h2 key='no-jobs'>No active jobs.</h2>)
		}

		let channels = [];
		Object.keys(this.state.channelList).forEach(key => {
      channels.push(<option key={key} value={key}>{this.state.channelList[key]}</option>);
		})

    return (
      <div className="App">
        {table}
        <hr/>
        <h1>Create a new job:</h1>
        <form onSubmit={this.createJob}>
          <p>
            Date: <input type="date" value={this.state.jobDate} onChange={(event) => {
              this.setState({jobDate: event.target.value});
            }}/>
          </p>
          <p>
            Time: <input type="time" value={this.state.jobTime} onChange={(event) => {
              this.setState({jobTime: event.target.value});
            }}/>
          </p>
          <p>
            Select channel: <select value={this.state.jobChannel != null ? this.state.jobChannel : 0} onChange={this.channelSelected}>
              {channels}
            </select>
          </p>
          <p>
            Message: <textarea rows='4' cols='80' value={this.state.jobMessage} onChange={(event) => {
              this.setState({jobMessage: event.target.value});
            }}/></p>
          <br/>
          <input type="submit" value="Create Job" />
        </form>
      </div>
    );
  }
}

export default App;
